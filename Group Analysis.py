#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

# Use seaborn style defaults and set the default figure size
sns.set(font_scale=1.5, rc={'figure.figsize':(20, 4)})


# In[2]:


df = pd.read_csv('AllTraffic_1.csv', sep='\t')
#df.head()


# In[3]:


ports_extra_df = pd.read_csv('ports.csv', sep=',')
ports_df = ports_extra_df[['port', 'description']].set_index('port')
ports_df.index = ports_df.index.astype(str)

#clean data
df['timestamp'] = pd.to_datetime(df['timestamp'])
df.index = df['timestamp']
df['actual_protocol'] = df['destination_port'].map(ports_df.to_dict()['description'])


# In[4]:


def scan_attack_grouper(ips, timeframe):
    dataframe = df.loc[df['source_ip'].isin(ips)]
    if timeframe:
        start_date, end_date = timeframe
        mask = (dataframe['timestamp'] > start_date) & (dataframe['timestamp'] <= end_date)
        dataframe = dataframe.loc[mask]

    scan_attack_ip_grouped = dataframe.groupby(
        'source_ip'
    ).resample('30T')['id'].nunique().to_frame(name='attacks')
    scan_attack_ip_grouped = scan_attack_ip_grouped.loc[(scan_attack_ip_grouped!=0).any(axis=1)].unstack('source_ip')
    return scan_attack_ip_grouped, dataframe


# ### Group 1
# #### the 85.93.20.0/24 subnet

# In[5]:


group1_df = df[df['subnet'] == '85.93.20.0/24']


# In[6]:


len(df[df['subnet'] == '85.93.20.0/24']['source_ip'].unique())


# In[7]:


ips = group1_df['source_ip'].unique()

start_date = pd.to_datetime('2019-04-22')
end_date = pd.to_datetime('2019-04-26')
timeframe = None #[start_date, end_date]
scan_attack_ip_group_4, scan_attack_ip_df_4 = scan_attack_grouper(ips, timeframe)

fg = scan_attack_ip_group_4.plot(marker='.', linestyle='None', legend=None)
for p in fg.patches:
    fg.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), rotation=45)
plt.gcf().axes[0].yaxis.get_major_formatter().set_scientific(False)
plt.xlabel('Time')
plt.ylabel('Attacks Per 30 minutes')
plt.title('Timeseries group 1')
plt.show()


# Looks as though these ips have scanned a large number of ports.

# In[8]:


group1_ports = group1_df['destination_port'].unique()
group1_ports = group1_ports.astype('int32')
group1_ports.min(), group1_ports.max(), len(group1_ports)


# 3306 is mySQL's default port

# In[9]:


# pd.set_option('display.max_rows', None)
# group1_df.groupby('destination_port')['id'].nunique().sort_values(ascending=False)


# This is the ip above that sent a huge number of packets on the 22 of april 2019

# In[10]:


# df[df['source_ip'] == '85.93.20.6'].groupby('destination_port')['id'].nunique().sort_values(ascending=False)


# In[11]:


attacker = df[df['source_ip'] == '85.93.20.6']
start_date = pd.to_datetime('2019-04-22')
end_date = pd.to_datetime('2019-04-26')
mask = (attacker['timestamp'] > start_date) & (attacker['timestamp'] <= end_date)
attacker_ports = attacker.loc[mask]['destination_port'].unique()
attacker_ports = attacker_ports.astype('int32')
attacker_ports.sort()
attacker_ports


# ### Group 2
# #### the 164.52.0.0/24 subnet

# In[12]:


group2_df = df[df['subnet'] == '164.52.0.0/24']
ips = group2_df['source_ip'].unique()
# print(ips)

start_date = pd.to_datetime('2019-04-22')
end_date = pd.to_datetime('2019-04-26')
timeframe = None #[start_date, end_date]
scan_attack_ip_group_4, scan_attack_ip_df_4 = scan_attack_grouper(ips, timeframe)

fg = scan_attack_ip_group_4.plot(marker='.', linestyle='None', legend=None)
for p in fg.patches:
    fg.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), rotation=45)
plt.gcf().axes[0].yaxis.get_major_formatter().set_scientific(False)
plt.xlabel('Time')
plt.ylabel('Attacks Per 30 minutes')
plt.title('Timeseries group 1')
# plt.show()


# In[13]:


# group2_df.groupby('destination_port')['id'].count().sort_values(ascending=False)


# ### Group 3
# 

# In[14]:


ips = ['124.172.138.143', '134.184.53.153', '181.214.87.34']
group3_df = df[df['source_ip'].isin(ips)]

start_date = pd.to_datetime('2018-04-20')
end_date = pd.to_datetime('2018-05-15')
timeframe = [start_date, end_date]
scan_attack_ip_group_4, scan_attack_ip_df_4 = scan_attack_grouper(ips, timeframe)

fg = scan_attack_ip_group_4.plot(marker='.', linestyle='None', legend=None)
for p in fg.patches:
    fg.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), rotation=45)
plt.gcf().axes[0].yaxis.get_major_formatter().set_scientific(False)
plt.xlabel('Time')
plt.ylabel('Attacks Per 30 minutes')
plt.title('Timeseries group 1')
# plt.show()


# In[15]:


group3_df['destination_ip'].unique()


# In[16]:


group3_ports = group3_df['destination_port'].unique()
group3_ports = group3_ports.astype('int32')
group3_ports.sort()
print(group3_ports.min(), group3_ports.max(), len(group3_ports))
# group3_ports


# ### Group 4

# In[17]:


ips = ['193.201.224.206', '195.3.147.49']
group4_df = df[df['source_ip'].isin(ips)]

start_date = pd.to_datetime('2018-04-20')
end_date = pd.to_datetime('2018-05-15')
timeframe = None #[start_date, end_date]
scan_attack_ip_group_4, scan_attack_ip_df_4 = scan_attack_grouper(ips, timeframe)

fg = scan_attack_ip_group_4.plot(marker='.', linestyle='None', legend=None)
for p in fg.patches:
    fg.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), rotation=45)
plt.gcf().axes[0].yaxis.get_major_formatter().set_scientific(False)
plt.xlabel('Time')
plt.ylabel('Attacks Per 30 minutes')
plt.title('Timeseries group 1')
plt.show()


# In[18]:


group4_df['destination_ip'].unique()


# In[19]:


group4_ports = group4_df['destination_port'].unique()
group4_ports = group4_ports.astype('int32')
group4_ports.sort()
print(group4_ports.min(), group4_ports.max(), len(group4_ports))
group4_ports


# ### Group 5

# In[20]:


ips = ['121.18.238.109', '221.194.44.231']
group5_df = df[df['source_ip'].isin(ips)]

start_date = pd.to_datetime('2017-02-15')
end_date = pd.to_datetime('2017-05-01')
timeframe = [start_date, end_date]
scan_attack_ip_group_4, scan_attack_ip_df_4 = scan_attack_grouper(ips, timeframe)

fg = scan_attack_ip_group_4.plot(marker='.', linestyle='None', legend=None)
for p in fg.patches:
    fg.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), rotation=45)
plt.gcf().axes[0].yaxis.get_major_formatter().set_scientific(False)
plt.xlabel('Time')
plt.ylabel('Attacks Per 30 minutes')
plt.title('Timeseries group 1')
# plt.show()


# In[21]:


group5_ports = group5_df['destination_port'].unique()
group5_ports = group5_ports.astype('int32')
group5_ports.sort()
print(group5_ports.min(), group5_ports.max(), len(group5_ports))
group5_ports


# In[22]:


len(group5_df)


# In[23]:


group5_df['destination_ip'].unique()


# In[24]:


group5_df.groupby('destination_ip').agg({
    'id': lambda i: i.nunique(),
    'source_ip': lambda ip: list(ip.unique()),
})


# In[25]:


cowrie_df = pd.read_csv('cowrie_1.csv', sep='\t')
cowrie_df['timestamp'] = pd.to_datetime(cowrie_df['timestamp'])


# In[26]:


cowrie_df[cowrie_df['source_ip'].isin(ips)]['credentials'].unique()


# In[27]:


cowrie_df[cowrie_df['source_ip'].isin(ips)]['loggedin'].unique()

