#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
import pandas as pd
import numpy as np
import seaborn
import matplotlib.pyplot as plt
import math
from datetime import timedelta
from collections import Counter
#from sklearn.metrics.pairwise import cosine_similarity
import pickle

DAY_MINIMUM = 10
ATTACK_MINIMUM = 100
SIMILARITY_MINIMUM = 0.9
# set to None to run on full dataset
SAMPLE_SIZE = None

SAVE_CLUSTERS = True
LOAD_CLUSTERS = False
LOAD_GRAPH_DF = False
SAVE_GRAPH_DF = True
SAVE_CLUSTER_GRAPHS = True

def df_read():
    print("reading csv")
    df = pd.read_csv('AllTraffic_1.csv', sep='\t')
    
    ### Data cleaning
    print("moving to timeseries index")
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df.index = df['timestamp']
    return df

def make_sample_df(df, size, random):
    if size == None:
        print("using full dataset")
        return df
    print("making sample_df")
    if random:
        sample_df = df.sample(size)
    else:
        sample_df = df[:size]
    return sample_df

def make_graph_df(sample_df):
    print("making graph dataframe")
    graph_df = sample_df.groupby('source_ip').resample('D')['id'].nunique().replace(0, np.nan).dropna()
    if SAVE_GRAPH_DF:
        print("saving graph_df")
        with open('graph_df.pickle', 'wb') as f:
            pickle.dump(graph_df, f)
    return graph_df

def load_graph_df():
    print("loading saved graph dataframe")
    with open('graph_df.pickle', 'rb') as f:
        return pickle.load(f)

def counter_cosine_similarity(c1, c2):
    terms = set(c1).union(c2)
    dotprod = sum(c1.get(k, 0) * c2.get(k, 0) for k in terms)
    magA = math.sqrt(sum(c1.get(k, 0)**2 for k in terms))
    magB = math.sqrt(sum(c2.get(k, 0)**2 for k in terms))
    return dotprod / (magA * magB)

#def make_timestamp_count_array(timestamp_counts):
#    rtn_array = empty_array.copy()
#    for timestamp, count in timestamp_counts.items():
#        rtn_array[0][(timestamp - first_day).days] = count
#    return rtn_array

def graph_clusters(clusters, graph_df):
    if SAVE_CLUSTER_GRAPHS:
        print("making directory for cluster graphs")
        Path("clusters").mkdir(exist_ok=True)
    print("making graphs")
    # Use seaborn style defaults and set the default figure size
    seaborn.set(rc={'figure.figsize':(11, 4)})
    for cluster in clusters:
        ip_sim_df = graph_df[graph_df.index.get_level_values(0).isin(cluster)]
        _fg = plt.figure()
        ip_sim_df.unstack(0).plot(marker='.', linestyle='None')
        plt.xlabel('Time')
        plt.ylabel('Attacks per Day')
        if SAVE_CLUSTER_GRAPHS:
            plt.savefig(f'clusters/{cluster[0]}.png', bbox_inches='tight')
        #plt.show()
        plt.close('all')

def load_clusters():
    print("loading saved clusters")
    with open('clusters.pickle', 'rb') as f:
        return pickle.load(f)

def make_clusters(similarity_dict):
    print("making clusters")
    key_buff = []
    clusters = []
    for k, v in similarity_dict.items():
        if k not in key_buff:
            ips = list(v.keys())
            ips.append(k)
            clusters.append(ips)
            key_buff.append(k)
        for kk in v.keys():
            if kk not in key_buff:
                key_buff.append(kk)
    
    if SAVE_CLUSTERS:
        if len(clusters) >= 1:
            with open('clusters.pickle', "wb") as f:
                pickle.dump(clusters, f)
    return clusters

def run_similarity(sample_df):
    print("seting up for similarity run")
    first_day = sample_df['timestamp'].min().normalize()
    last_day = sample_df['timestamp'].max().normalize()
    delta = (last_day - first_day).days
    days_array = [(first_day + timedelta(days=day)).strftime('%Y-%m-%d') for day in range(delta + 1)]
    _empty_array = np.array([[0] * len(days_array)])
    
    # sort by source ip and get unique timestamps the round them to nearest day
    timestamp_df = sample_df.groupby('source_ip')['timestamp'].unique().apply(lambda time_array: list(pd.to_datetime(time).normalize() for time in time_array))
    # remove source ips with less than ATTACK_MINIMUM
    timestamp_dict = timestamp_df.loc[lambda stamps_df: list(len(row) > ATTACK_MINIMUM for row in stamps_df)].to_dict()
    
    # count active days
    timestamp_dict.update((ip, Counter(timestamps)) for ip, timestamps in timestamp_dict.items())
    
    print("running similarity")
    similarity_dict = {}
    for ip, timestamp_counts in timestamp_dict.items():
        if len(timestamp_counts) < DAY_MINIMUM: continue
        ip_dict_buffer = {}
        #ip1_timestamp_array = make_timestamp_count_array(timestamp_counts)
        for ip2, timestamp_counts2 in timestamp_dict.items():
            if len(timestamp_counts2) < DAY_MINIMUM: continue
            if ip == ip2: continue
            #ip2_timestamp_array = make_timestamp_count_array(timestamp_counts2)
            similarity1 = counter_cosine_similarity(dict(timestamp_counts), dict(timestamp_counts2))
            #similarity2 = cosine_similarity(ip1_timestamp_array, ip2_timestamp_array)
            if similarity1 == 0: continue
            if ip2 in similarity_dict: continue
            if similarity1 > SIMILARITY_MINIMUM :
                ip_dict_buffer[ip2] = similarity1
                #print(f'Ip 1: {ip}\nIp 2: {ip2}\n\t{similarity1}\n\t{similarity2}')
        if len(ip_dict_buffer) >= 1: similarity_dict[ip] = ip_dict_buffer

    return similarity_dict
    


if LOAD_CLUSTERS and Path('clusters.pickle').is_file():
    clusters = load_clusters()
else:
    df = df_read()
    sample_df = make_sample_df(df, SAMPLE_SIZE, False)
    similarity_dict = run_similarity(sample_df)
    clusters = make_clusters(similarity_dict)

if LOAD_GRAPH_DF and Path('graph_df.pickle').is_file():
    graph_df = load_graph_df()
else: 
    graph_df = make_graph_df(sample_df)

print(f"sample size is: {SAMPLE_SIZE}\n"
      f"minimum number of days is: {DAY_MINIMUM}\n"
      f"minimum similarity is: {SIMILARITY_MINIMUM}\n"
      f"minimum attack count is: {ATTACK_MINIMUM}"
)

graph_clusters(clusters, graph_df)

