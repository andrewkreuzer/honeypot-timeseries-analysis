## Analysis of Honeypot Attack Data

### Description
This project aims to analyze honeypot attack data and derive meaning from the
large sets of data that honeypots produce.

### Getting Started

#### Dependencies

* Jupyter Notebooks
* python
* pandas
* numpy
* matplotlib
* seaborn
* openpyxl

all dependencies (except openpyxl) are include in Jupyter Notebooks scipy-notebook docker image.

#### Installing

##### git clone this repo
```
git clone https://gitlab.com/andrewkreuzer/honeypot-timeseries-analysis.git
```

##### download data
download or move these files into the git directory.
```
cd honeypot-timeseries-analysis
wget -O AllTraffic_1.csv https://f002.backblazeb2.com/file/honeypot-data/AllTraffic_1.csv
wget -O cowrie_1.csv https://f002.backblazeb2.com/file/honeypot-data/cowrie_1.csv
wget -O ports.csv https://f002.backblazeb2.com/file/honeypot-data/ports.csv
```

##### start Jupyter Notebook docker image
```
docker run --name honeypot_analysis -v path/to/git/directory:/home/jovyan/work -p 8888:8888 -d jupyter/scipy-notebook:latest
```
You will need to follow output of the docker image logs when above command is run with -d
```
docker logs -f honeypot_analysis
```

#### Executing program

Once the container is up and running the logs will output a link to the Jupyter 
Notebook dashboard page, `http://<hostname>:8888/?token=<token>`  where `hostname` 
is the name of the computer running docker and `token` is the secret token printed 
in the console. Notebook files can then be selected and ran individually.

### Authors
Andrew Kreuzer

